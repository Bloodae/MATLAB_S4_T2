Equilibre

%% Identification Fs
close all
showGraphs = 0;

load('Fs.mat');

as_data = (-1*ones(length(Fs),1)) ./ Fs;

aSp = polyfit(z_pos(1:end-155), as_data(1:end-155), 3);

if showGraphs == 1
    Fsp = (-1*ones(length(Fs),1)) ./ polyval(aSp, z_pos);
    
    figure(1)
    hold on
    plot(z_pos, Fs)
    plot(z_pos,Fsp)
    legend('show')
    legend('Fs', 'Fsp')
    axis([z_pos(1)-0.0005 z_pos(end)+0.0005 Fs(1)-1 Fs(end)+1])
    hold off
end

%Force des aimants permanents
aS0 = aSp(4);
aS1 = aSp(3);
aS2 = aSp(2);
aS3 = aSp(1);

%% Identification Fe
load('Fe_attraction.mat');

bE1 = 13.029359254409743;

i1 = -1;
i2 = -2;


num1 = (i1 + bE1*abs(i1))*sign(i1);
num2 = (i2 + bE1*abs(i2))*sign(i2);
aE_data1 = (num1*ones(length(Fe_m1A),1)) ./ Fe_m1A;
aE_data2 = (num2*ones(length(Fe_m2A),1)) ./ Fe_m2A;

aEp1 = polyfit(z_m1A, aE_data1, 3);
aEp2 = polyfit(z_m2A, aE_data2, 3);
%aEp2 = aEp1;
%aEp2(2) = sqrt(abs(-2))*aEp1(2);

if showGraphs == 1
    Fe_m1Ap = (num1*ones(length(Fe_m1A),1)) ./ polyval(aEp1, z_m1A);
    Fe_m2Ap = (num2*ones(length(Fe_m2A),1)) ./ polyval(aEp2, z_m2A);
    
    figure(2)
    hold on;
    plot(z_m1A, Fe_m1Ap)
    plot(z_m2A, Fe_m2Ap)
    plot(z_m1A, Fe_m1A)
    plot(z_m2A, Fe_m2A)
    legend('show')
    legend('-1Ap', '-2Ap', '-1A', '-2A')
    hold off
end

coeffsAEp = aEp1 - aEp2;

%Force des électroaimants
aE0 = aEp1(4);
aE1 = aEp1(3);
aE2 = aEp1(2);
aE3 = aEp1(1);




for i = 1:5
    syms IK_eqs
    F_eq = FZ_eq/3 ==(IK_eqs * abs(IK_eqs) + bE1 * IK_eqs) / (aE0 + aE1*Z_eq + aE2*Z_eq^2 + aE3*Z_eq^3) - 1 / (aS0 + aS1*Z_eq + aS2*Z_eq^2 + aS3*Z_eq^3);
    IK_eq = double(solve(F_eq, IK_eqs));
    aE0 = aEp1(4) + (IK_eq + 1)*coeffsAEp(4);
    aE1 = aEp1(3) + (IK_eq + 1)*coeffsAEp(3);
    aE2 = aEp1(2) + (IK_eq + 1)*coeffsAEp(2);
    aE3 = aEp1(1) + (IK_eq + 1)*coeffsAEp(1);
end

