Declaration_parametres
Equilibre

syms ZK IK;
PEK = aE0 + aE1*ZK + aE2*ZK^2 + aE3*ZK^3;
PEA = subs(PEK,ZK, ZA);
PEB = subs(PEK,ZK, ZB);
PEC = subs(PEK,ZK, ZC);

F_EK = (IK^2 + bE1*abs(IK))*sign(IK) / PEK;
F_EA = subs(F_EK, {ZK, IK}, {ZA, IA});
F_EB = subs(F_EK, {ZK, IK}, {ZB, IB});
F_EC = subs(F_EK, {ZK, IK}, {ZC, IC});

PSK = aS0 + aS1*ZK + aS2*ZK^2 + aS3*ZK^3;
PSA = subs(PSK, ZK, ZA);
PSB = subs(PSK, ZK, ZB);
PSC = subs(PSK, ZK, ZC);

F_SA = -1 / PSA;
F_SB = -1 / PSB;
F_SC = -1 / PSC;

%Force des actionneurs
FA = F_EA + F_SA;
FB = F_EB + F_SB;
FC = F_EC + F_SC;



%% �quations non lin�aires
FZ = FA + FB + FC;
g = 9.81;

%Plaque: Z

ACCZ = FZ / masseTot + g;
VZ = VZ;

%Plaque: rotation X
ALPHAx = 1/inertiePx * (YB * FB + YC * FC + Py * masseS * g);
Wx = Wx;

%Plaque: rotation Y
ALPHAy = 1/inertiePy * -(XB * FB + XC * FC + XA * FA + Px * masseS * g);
Wy = Wy;

%Sph�re: Mouvement en X

ACCx = Ay * (-1) * masseS * g / (masseS + inertieS/rS^2);
Vx = Vx;

%Sph�re: Mouvement en Y
ACCy = Ax * masseS * g / (masseS + inertieS/rS^2);
Vy = Vy;

% Actionneurs
dIA = 1/LA * (VA - RA * IA);
dIB = 1/LB * (VB - RB * IB);
dIC = 1/LC * (VC - RC * IC);


