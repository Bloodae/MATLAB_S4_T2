Equilibre
EQ_non_lineaires

U = [YA YB YC; -XA -XB -XC; 1 1 1];




%Polynome � l'�quilibre de la force des �lectroaimants
PEK_eq = aE0 + aE1*Z_eq + aE2*Z_eq^2 + aE3*Z_eq^3;

%Polynome � l'�quilibre de la force des aimants permanents
PSK_eq = aS0 + aS1*Z_eq + aS2*Z_eq^2 + aS3*Z_eq^3;

% Courants � l'�quilibre, m�thode it�rative

for i = 1:5
    syms IK_eqs
    F_eq = FZ_eq/3 ==(IK_eqs * abs(IK_eqs) + bE1 * IK_eqs) / (aE0 + aE1*Z_eq + aE2*Z_eq^2 + aE3*Z_eq^3) - 1 / (aS0 + aS1*Z_eq + aS2*Z_eq^2 + aS3*Z_eq^3);
    IK_eq = double(solve(F_eq, IK_eqs));
    aE0 = aEp1(4) + (IK_eq + 1)*coeffsAEp(4);
    aE1 = aEp1(3) + (IK_eq + 1)*coeffsAEp(3);
    aE2 = aEp1(2) + (IK_eq + 1)*coeffsAEp(2);
    aE3 = aEp1(1) + (IK_eq + 1)*coeffsAEp(1);
end

VA_eq = double(RA * IK_eq);
VB_eq = double(RB * IK_eq);
VC_eq = double(RC * IK_eq);

% Forces � l'�quilibre
Fs_eq = -1./(aS0+aS1*Z_eq+aS2*Z_eq.^2+aS3*Z_eq.^3);

aFe_eq = (aE0+aE1*Z_eq+aE2*Z_eq.^2+aE3*Z_eq.^3);
bFe_eq = -(IK_eq+bE1);

Fe_eq = bFe_eq./aFe_eq;
FK_eq = Fs_eq + Fe_eq;

%
% Affichage des valeurs
%

% Entr�es
disp('===== Entr�es � l''�quilibre =====')
disp(['Va_eq = ' num2str(VA_eq) ' V'])
disp(['Vb_eq = ' num2str(VB_eq) ' V'])
disp(['Vc_eq = ' num2str(VC_eq) ' V'])
disp(' ')

% Variables d'�tats
disp('===== Variables d''�tats � l''�quilibre =====')
disp(['Ax_eq = ' num2str(Ax_eq) ' rad'])
disp(['Ay_eq = ' num2str(Ax_eq) ' rad'])
disp(['Z_eq = ' num2str(Z_eq) ' m'])
disp(['Wx_eq = ' num2str(0) ' rad/s'])
disp(['Wy_eq = ' num2str(0) ' rad/s'])
disp(['Vz_eq = ' num2str(0) ' m/s'])
disp(['Px_eq = ' num2str(0) ' m'])
disp(['Py_eq = ' num2str(0) ' m'])
disp(['Vx_eq = ' num2str(0) ' m'])
disp(['Vy_eq = ' num2str(0) ' m'])
disp(['IA_eq = ' num2str(IK_eq) ' A'])
disp(['IB_eq = ' num2str(IK_eq) ' A'])
disp(['IC_eq = ' num2str(IK_eq) ' A'])
disp(' ')

% Sorties
disp('===== Sorties � l''�quilibre =====')
disp(['dD_eq = ' num2str(Z_eq) ' m'])
disp(['dE_eq = ' num2str(Z_eq) ' m'])
disp(['dF_eq = ' num2str(Z_eq) ' m'])
disp(['Px_eq = ' num2str(0) ' m'])
disp(['Py_eq = ' num2str(0) ' m'])
disp(['Vx_eq = ' num2str(0) ' m/s'])
disp(['Vy_eq = ' num2str(0) ' m/s'])
disp(' ')

% Forces
disp('===== Force � l''�quilibre =====')
disp(['Fe_eq = ' num2str(Fe_eq)])
disp(['Fs_eq = ' num2str(Fs_eq)])
disp(' ')

%% Lin�arisation
syms deltaIA deltaIB deltaIC deltaIK deltaPy deltaPx deltaAx deltaAy deltaZ XK YK;

% ACTUATEURS
% dFKdZK = -(IK * abs(IK) + bE1*IK) * (aE1 + 2*aE2*ZK + 3 * aE3 * ZK^2)/ PEK^2 - (aS1 + 2*aS2*ZK + 3 * aS3*ZK^2) / PSK^2;
dFKdAx = (IK_eq * abs(IK_eq) + bE1*IK_eq) * (-YK) * (aE1 + 2*aE2*Z_eq + 3 * aE3 * Z_eq^2) / (PEK_eq)^2 + YK * (aS1 + 2*aS2*Z_eq + 3 * aS3*Z_eq^2) / (PSK_eq)^2;
dFKdAy = subs(dFKdAx, {YK}, {-XK});
dFKdZ = subs(dFKdAx, {YK}, {1});
dFKdIK = (abs(IK_eq) + sign(IK_eq) * IK_eq + bE1) / PEK_eq;


% ALPHAx = f(Ax, Ay, Z, IB, IC, Py)
deltaALPHAx =               1/inertiePx * (YB * subs(dFKdAx, {YK}, {YB}) + YC * subs(dFKdAx, {YK}, {YC})) * deltaAx;
deltaALPHAx = deltaALPHAx + 1/inertiePx * (YB * subs(dFKdAy, {XK}, { XB}) + YC * subs(dFKdAy, {XK}, {XC})) * deltaAy;
deltaALPHAx = deltaALPHAx + 1/inertiePx * (YB * dFKdZ + YC * dFKdZ) * deltaZ;
deltaALPHAx = deltaALPHAx + 1/inertiePx * (YB * dFKdIK * deltaIB + YC * dFKdIK * deltaIC);
deltaALPHAx = deltaALPHAx + 1/inertiePx * masseS * g * deltaPy;

% ALPHAy = f(Ax, Ay, Z, IA, IB, IC, Px)
deltaALPHAy =          -1/inertiePx * (XA * subs(dFKdAx, {YK}, {YA}) + XB * subs(dFKdAx, {YK}, {YB}) + XC * subs(dFKdAx, {YK}, {YC})) * deltaAx;
deltaALPHAy = deltaALPHAy - 1/inertiePx * (XA * subs(dFKdAy, {XK}, {XA}) + XB * subs(dFKdAy, {XK}, {XB}) + XC * subs(dFKdAy, {XK}, {XC})) * deltaAy;
deltaALPHAy = deltaALPHAy - 1/inertiePx * (XA * dFKdZ + XB * dFKdZ + XC * dFKdZ) * deltaZ;
deltaALPHAy = deltaALPHAy - 1/inertiePx * (XA * dFKdIK * deltaIA + XB * dFKdIK* deltaIB + XC * dFKdIK * deltaIC)     ;
deltaALPHAy = deltaALPHAy - 1/inertiePx * masseS * g * deltaPx;

%ACCZ = f(Ax, Ay, Z, IA, IB, IC)
deltaACCZ =         (subs(dFKdAx, {YK}, {YA}) + subs(dFKdAx, {YK}, {YB}) + subs(dFKdAx, {YK}, {YC})) * deltaAx;
deltaACCZ = deltaACCZ + (subs(dFKdAy, {XK}, {XA}) + subs(dFKdAy, {XK}, {XB}) + subs(dFKdAy, {XK}, {XC})) * deltaAy;
deltaACCZ = deltaACCZ + (dFKdZ + dFKdZ + dFKdZ)     * deltaZ;
deltaACCZ = deltaACCZ + (dFKdIK * deltaIA + dFKdIK* deltaIB + dFKdIK* deltaIC);
deltaACCZ = deltaACCZ / masseTot;


%% Forme matricielle

% Matrice des variables d'�tat
syms deltaWx deltaWy deltaVZ deltaVx deltaVy deltaVA deltaVB deltaVC
deltax_ = [deltaAx; deltaAy; deltaZ; deltaWx; deltaWy; deltaVZ; deltaPx; deltaPy; deltaVx; deltaVy; deltaIA; deltaIB; deltaIC];
deltau_ABC = [deltaVA; deltaVB; deltaVC];


% Construction de la matrice A ligne par ligne
deltaWx_matA = [zeros(1, 3) 1 zeros(1, 9)];
deltaWy_matA = [zeros(1, 4) 1 zeros(1, 8)];
deltaVZ_matA = [zeros(1, 5) 1 zeros(1, 7)];
deltaALPHAx_matA = equationsToMatrix(deltaALPHAx, [deltaAx deltaAy deltaZ deltaWx deltaWy deltaVZ deltaPx deltaPy deltaVx deltaVy deltaIA deltaIB deltaIC]);
deltaALPHAy_matA = equationsToMatrix(deltaALPHAy, [deltaAx deltaAy deltaZ deltaWx deltaWy deltaVZ deltaPx deltaPy deltaVx deltaVy deltaIA deltaIB deltaIC]);
deltaACCZ_matA = equationsToMatrix(deltaACCZ, [deltaAx deltaAy deltaZ deltaWx deltaWy deltaVZ deltaPx deltaPy deltaVx deltaVy deltaIA deltaIB deltaIC]);
deltaVx_matA = [zeros(1, 8) 1 zeros(1, 4)];
deltaVy_matA = [zeros(1, 9) 1 zeros(1, 3)];
deltaACCx_matA = [0 ((-1) * masseS * g / (masseS + inertieS/rS^2)) zeros(1, 11)];
deltaACCy_matA = [(masseS * g / (masseS + inertieS/rS^2)) zeros(1, 12)];
deltadIA_matA = [zeros(1, 10) (- RA / LA) zeros(1, 2)];
deltadIB_matA = [zeros(1, 11) (- RB / LB) zeros(1, 1)];
deltadIC_matA = [zeros(1, 12) (- RC / LC) ];

A_ = double([deltaWx_matA; deltaWy_matA; deltaVZ_matA; 
     deltaALPHAx_matA; deltaALPHAy_matA; deltaACCZ_matA; 
     deltaVx_matA; deltaVy_matA; deltaACCx_matA; deltaACCy_matA; 
     deltadIA_matA; deltadIB_matA; deltadIC_matA]);



% Construction de la matrice B ligne par ligne
B_ = zeros(10, 3);
deltaIA_matB = [(1 / LA) 0 0];
deltaIB_matB = [0 (1 / LB) 0];
deltaIC_matB = [0 0 (1 / LC)];

B_ = double([B_; deltaIA_matB; deltaIB_matB; deltaIC_matB]);

C_ = double([TDEF' zeros(3,10); zeros(4,6) eye(4) zeros(4,3)]);

D_ = zeros(7,3);

PP_ = A_(4:6, 1:3);
PS_ = A_(4:6,7:8);
PC_ = A_(4:6,11:13);
SP_ = A_(9:10,1:3);
CC_ = A_(11:13,11:13);
CV_ = B_(11:13,1:3);