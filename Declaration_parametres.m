
format shortEng
digits(6);


%% D�claration de variables


Equilibre
% Force des �lectroaimants
Identification_actionneurs

%Plaque: angle, vitesse angulaire, inertie, hauteur et vitesse verticale
syms Ax Ay Wx Wy Z VZ;


%Sph�re: Position, vitesse, masse
syms Px Py Pz Vx Vy Vz;

%Actionneurs: Force, moment, tension, courant
syms IA IB IC VA VB VC;

%D�riv�es des variables d'�tat
syms ALPHAx ALPHAy ACCZ ACCx ACCy dIA dIB dIC



%% D�claration de matrices

%Matrice des variables d'entr�e
u_ABC = [VA; VB; VC];

%Matrice des variables d'�tat
x_ = [Ax; Ay; Z; Wx; Wy; VZ; Px; Py; Vx; Vy; IA; IB; IC];

%Matrice des d�riv�es des variables d'�tat
dx_ = [Wx; Wy; VZ; ACCx; ALPHAy; ACCZ; Vx; Vy; ACCx; ACCy; dIA; dIB; dIC];





