<<<<<<< HEAD
FT_et_stabilite
clc
t = [0:0.01:150]';   
u = ones(size(t));


% sp�cifications
ts_s_min = 2  ;%   secondes � 2%
ts_s     = 3  ;%   secondes � 2%
ts_s_max = 4  ;%   secondes � 2%
zeta_s   = 0.9;


% conception du compensateur
wn_s = 4/(ts_s_min*zeta_s);

pole_p = -(zeta_s*wn_s) + j*wn_s*sqrt(1 - zeta_s^2);%-----------------------> si ts = 2s, pole_p = -2.0000e+000 +968.6442e-003i  
                                                    %-----------------------> si ts = 3s, pole_p = -1.3333e+000 +645.7628e-003i                             
pole_n = -(zeta_s*wn_s) - j*wn_s*sqrt(1 - zeta_s^2);

figure('name','Sphere y p�les d�sir�s')
rlocus(hsy)
hold on
plot(pole_p,'p')
plot(pole_n,'p')
title('Lieu des racines de la position de la sph�re en y')
axis([-3 0.1 -1 1])
hold off

% cr�ation de d'un AvPh ===================================================
% recherche de la phase � ajouter
[num_hsy,den_hsy] = tfdata(hsy,'v');
phase_manquante  = rad2deg(angle(polyval(num_hsy,pole_p)./polyval(den_hsy,pole_p)))-360;%----> phase_manquante = -308.3161
deltaphi = -180 - phase_manquante;%----------------------------------------------------------> deltaphi        = 128.3161

%recherche de la position du zero et du pole du compensateur
alpha = 180 - atand(imag(pole_p)./abs(real(pole_n)));%-----------------------> alpha   = 154.1581
phizero = (alpha + deltaphi)./2;%--------------------------------------------> phizero = 141.2371
phipole = (alpha - deltaphi)./2;%--------------------------------------------> phipole = 12.9210
Z = -zeta_s*wn_s + imag(pole_p)./tand(180-phizero);%-------------------------> Z       = -793.6508 
P = -zeta_s*wn_s - imag(pole_p)./tand(phipole);%-----------------------------> P       = -6.2222


% recherche du gain Ky
as = (pole_p-Z)./(pole_p-P);
hsyd = polyval(num_hsy,pole_p)./polyval(den_hsy,pole_p);
Kpy = 1./abs(as * hsyd);%-----------------------------------------------------> Kpy = 1.9733
soutf = tf([1 -Z],[1 -P]);
APy = Kpy*soutf;%-------------------------------------------------------------> fonction de transfert de l'avance de phase


% fonction de transfert compens� FTBO =====================================
APyhsy = APy*hsy;%-----------------------------------------------------------> fonction de transfert du syst�me en boucle ouverte 
%fonction de transfert compens� FTBF
[numhsyF,denhsyF] = feedback(APyhsy,1);
APyhsyF = tf(numhsyF,denhsyF); %---------------------------------------------> fonction de transfert du syst�me en boucle ferm�e


% Lieu des racines de APyhsy
figure('name','Lieu des racines de l AvPh avec le syst�me')
rlocus(APyhsy)
hold on
plot(pole_p,'p')
plot(pole_n,'p')
p = rlocus(APyhsy,1);
plot(p,'s')
axis([-3 0.1 -1 1])
hold off

% lieu de bode du syst�me compens�
figure ('name','lieu de bode du syst�me hsy compens�')
margin(APyhsy)
[GM,PM,wp,wg] = margin(APyhsy);
grid


%R�ponses du syst�me
y0 = lsim(feedback(hsy,1),u,t); % sumulation de hsy sans compensation
y1 = lsim(APyhsyF,u,t);         % sumulation de hsy avec compensation

figure('name','r�ponses de hsy avec et sans AvPh') %-------------------------> stabilise � 3.29s
plot(t, y0, 'm', 'linewidth',1)
hold on
plot(t, y1, 'b', 'linewidth',1)
axis([0 10 0 1.4]) 
erreurhaut = 1.02*ones(size(t));
erreurbas = 0.98*ones(size(t));
ploterreurhaut = line(t,erreurhaut,'linestyle','--','color','r');
ploterreurbas = line(t,erreurbas,'linestyle','--','color','r');
legend('hsy sans AvPh','hsy avec AvPh','erreur � 2%')
hold off

=======
FT_et_stabilite
clc
t = [0:0.01:150]';   
u = ones(size(t));


% sp�cifications
ts_s_min = 2  ;%   secondes � 2%
ts_s     = 3  ;%   secondes � 2%
ts_s_max = 4  ;%   secondes � 2%
zeta_s   = 0.9;


% conception du compensateur
wn_s = 4/(ts_s_min*zeta_s);

pole_p = -(zeta_s*wn_s) + j*wn_s*sqrt(1 - zeta_s^2);%-----------------------> si ts = 2s, pole_p = -2.0000e+000 +968.6442e-003i  
                                                    %-----------------------> si ts = 3s, pole_p = -1.3333e+000 +645.7628e-003i                             
pole_n = -(zeta_s*wn_s) - j*wn_s*sqrt(1 - zeta_s^2);

figure('name','Sphere y p�les d�sir�s')
rlocus(hsy)
hold on
plot(pole_p,'p')
plot(pole_n,'p')
title('Lieu des racines de la position de la sph�re en y')
axis([-3 0.1 -1 1])
hold off

% cr�ation de d'un AvPh ===================================================
% recherche de la phase � ajouter
[num_hsy,den_hsy] = tfdata(hsy,'v');
phase_manquante  = rad2deg(angle(polyval(num_hsy,pole_p)./polyval(den_hsy,pole_p)))-360;%----> phase_manquante = -308.3161
deltaphi = -180 - phase_manquante;%----------------------------------------------------------> deltaphi        = 128.3161

%recherche de la position du zero et du pole du compensateur
alpha = 180 - atand(imag(pole_p)./abs(real(pole_n)));%-----------------------> alpha   = 154.1581
phizero = (alpha + deltaphi)./2;%--------------------------------------------> phizero = 141.2371
phipole = (alpha - deltaphi)./2;%--------------------------------------------> phipole = 12.9210
Z = -zeta_s*wn_s + imag(pole_p)./tand(180-phizero);%-------------------------> Z       = -793.6508 
P = -zeta_s*wn_s - imag(pole_p)./tand(phipole);%-----------------------------> P       = -6.2222


% recherche du gain Ky
as = (pole_p-Z)./(pole_p-P);
hsyd = polyval(num_hsy,pole_p)./polyval(den_hsy,pole_p);
Kpy = 1./abs(as * hsyd);%-----------------------------------------------------> Kpy = 1.9733
soutf = tf([1 -Z],[1 -P]);
APy = Kpy*soutf;%-------------------------------------------------------------> fonction de transfert de l'avance de phase


% fonction de transfert compens� FTBO =====================================
APyhsy = APy*hsy;%-----------------------------------------------------------> fonction de transfert du syst�me en boucle ouverte 
%fonction de transfert compens� FTBF
[numhsyF,denhsyF] = feedback(APyhsy,1);
APyhsyF = tf(numhsyF,denhsyF); %---------------------------------------------> fonction de transfert du syst�me en boucle ferm�e


% Lieu des racines de APyhsy
figure('name','Lieu des racines de l AvPh avec le syst�me')
rlocus(APyhsy)
hold on
plot(pole_p,'p')
plot(pole_n,'p')
p = rlocus(APyhsy,1);
plot(p,'s')
axis([-3 0.1 -1 1])
hold off

% lieu de bode du syst�me compens�
figure ('name','lieu de bode du syst�me hsy compens�')
margin(APyhsy)
[GM,PM,wp,wg] = margin(APyhsy);
grid


%R�ponses du syst�me
y0 = lsim(feedback(hsy,1),u,t); % sumulation de hsy sans compensation
y1 = lsim(APyhsyF,u,t);         % sumulation de hsy avec compensation

figure('name','r�ponses de hsy avec et sans AvPh') %-------------------------> stabilise � 3.29s
plot(t, y0, 'm', 'linewidth',1)
hold on
plot(t, y1, 'b', 'linewidth',1)
axis([0 10 0 1.4]) 
erreurhaut = 1.02*ones(size(t));
erreurbas = 0.98*ones(size(t));
ploterreurhaut = line(t,erreurhaut,'linestyle','--','color','r');
ploterreurbas = line(t,erreurbas,'linestyle','--','color','r');
legend('hsy sans AvPh','hsy avec AvPh','erreur � 2%')
hold off

>>>>>>> devMAL
