% Conception de l'asservissement pour la fonction de transfert en hauteur
% de la plaque

% Philippe Martel-Dion
% v1 - 9 avril 2017

FT_et_stabilite
%% Conception des compensateurs
close all
clc

% Fonction de transfert:
FTz = hpz;
[numz, denz] = tfdata(FTz, 'v');
s = tf('s');

% Sp�cifications d�sir�es
PM_des = 25; % deg
wg_des = 185; %rad/s
erp1 = 0.0004;
erp2 = 0;

mrg = 7; % Marge pour ajuster la marge de phase apres le retard ou PI

% Specs de s�curit�
GM_sec = 10; % dB
PM_sec = 25; % deg

% D�termination des compensateurs � r�aliser
figure(1); hold on
margin(numz, denz)

% Avance de phase avec Bode
Kdes_av = 1/abs(polyval(numz, 1i*wg_des)/polyval(denz, 1i*wg_des));

figure(1)
margin(Kdes_av*numz, denz)
[~, PM_now, ~, wg_now] = margin(Kdes_av*numz, denz);

dphi = PM_des - PM_now + mrg;
phi2 = dphi/2;

alpha = (1-sind(phi2))/(1+sind(phi2));

Ta = 1/(wg_des*sqrt(alpha));

Ka = Kdes_av/(alpha);

AvPhz = Ka*(s+1/Ta)^2/(s+1/(alpha*Ta))^2;

FTz_a = FTz*AvPhz;
figure(1)
margin(FTz_a)

figure(2); hold on
nyquist(FTz_a)

% Cas 1: retard de phase
Kpos_des = 1/erp1 - 1;
Kpos = numz(end)/denz(end);
Kdes_re = Kpos_des/Kpos;

beta = Kdes_re;

Tr = 10/wg_des;
zr = 1/Tr;
pr = 1/(beta*Tr);

Kr = 1;

RePhz = Kr*(s + zr)/(s + pr);

FTz_ar = FTz_a*RePhz;

figure(1)
margin(FTz_ar)

figure(2)
nyquist(FTz_ar)

h_comp_z1 = AvPhz * RePhz

% Cas 2: PI
zi = wg_des/10;

Ki = 1;

PIz = Ki*(s + zi)/s;

FTz_ai = FTz_a*PIz;

figure(1)
margin(FTz_ai)

figure(2)
nyquist(FTz_ai)

h_comp_z2 = AvPhz * PIz

% Formattage des figures
lgnd1 = {'Syst�me original','Syst�me compens� K^*'};
lgnd2 = {'Syst�me compens� AvPh^2','Syst�me compens� AvPh^2 + RePh', 'Syst�me compens� AvPh^2 + PI'};

figure(1)
legend([lgnd1 lgnd2], 'Location', 'best')

figure(2)
legend(lgnd2, 'Location', 'best')
axis([-8, 1, -2, 2])

% V�rification des fr�quence des p�les et z�ros
figure(3)
rlocus(FTz_ai, 1, 'b')
rlocus(FTz_ar, 1, 'r')

% R�ponse en transitoire
figure
step(feedback(FTz*h_comp_z2,1))

% Valeurs pour le banc d'essai
testdiscret(AvPhz*PIz)
