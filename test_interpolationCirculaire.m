close all
clear all
clc

load('trajectoire.mat')
xy = [NAB(1:end-1,:); NBA(1:end-1,:)];
v = vAB;
ts = Ts;

[Pi, Ltr, E, Vr, Traj, tt, deplacement] = InterpolationCirculaire(xy,v,ts);

figure(1)
hold on
plot(Traj(:,1),Traj(:,2),'s')
plot(xy(:,1),xy(:,2),'p')
hold off


dxy = [diff(deplacement(:,2)) diff(deplacement(:,3))];
dsdt = [v; sqrt(dxy(:,1).^2+dxy(:,2).^2)./ts];

figure(3)
plot(deplacement(:,1), dsdt)